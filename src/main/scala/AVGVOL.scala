//@begin_imports
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
//@end_imports

//@begin_methods

object AVGVOL {

  //@end_methods

  //@begin_setup
  def main(args: Array[String]) {

    //@end_setup
    //@begin_spark_prog

    val conf = new SparkConf().setAppName("AVGVOL")
    val sc = new SparkContext(conf)

    val lines = sc.textFile(args(0))

    val dateValueMap = lines.map(line=>(line.split(",")(1), line.split(",")(6).toInt))
    val mapOnes = dateValueMap.map(tuple => (tuple._1, (tuple._2, 1)))
    val reduceRDD = mapOnes.reduceByKey((t1,t2) => (t1._1+t2._1, t2._2+t1._2))
    val avgResult = reduceRDD.map(t => (t._1, t._2._1/t._2._2))


    //@end_spark_prog

    println(avgResult.count())

  }
}