import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import scala.math.pow
import java.io.{FileWriter}

object ekwtdm {
def maxString(one: String, two: String): String ={
    if(one.compareTo(two)>0)
      return one
    return two
  }


def generateWindow(tuple:((String, String, Double), Long), windowSize: Int): TraversableOnce[((Int, String),(String, Double, Int))] ={
    val name = tuple._1._1
    val date = tuple._1._2
    val value = tuple._1._3
    val index = tuple._2.toInt
    return for(offset  <- 0 until windowSize) yield
      ((index-offset, name), (date, value,1))
  }



def main(args: Array[String]) {

val  WINDOW_SIZE = 20

	val sc = new SparkContext()

	val lines = sc.textFile(args(0))
	val dateValueMap = lines.map(line=>(line.split(",")(0), line.split(",")(1), line.split(",")(5).toDouble))
	val putIndex = dateValueMap.zipWithIndex()
	val slidingWindow = putIndex.flatMap(tuple => generateWindow(tuple, WINDOW_SIZE))
	val reduceRDD = slidingWindow.reduceByKey((t1,t2)=>(maxString(t1._1, t2._1),t1._2+t2._2,t1._3+t2._3))
	val filterRDD = reduceRDD.filter(t1=> (t1._2._3 == WINDOW_SIZE)).cache()

	val mapRDDMAE = filterRDD.map(t1=>((t1._1._2, t1._2._1), ((t1._2._2/t1._2._3.toDouble)-(t1._2._2/t1._2._3.toDouble)*0.05, (t1._2._2/t1._2._3.toDouble)+(t1._2._2/t1._2._3.toDouble)*0.025)))
	val sortedRDDMAE = mapRDDMAE.sortByKey()
  //  for (x <- sortedRDDMAE.collect())
  //    println(x)
	new FileWriter("MAE.scala.output.txt",true) { write(sortedRDDMAE.count()+"\n"); close }

	val mapRDDSMA = filterRDD.map(t1=>((t1._1._2, t1._2._1), (t1._2._2/t1._2._3.toDouble)))
	val sortedRDDSMA = mapRDDSMA.sortByKey()
	new FileWriter("SMA20.scala.output.txt",true) { write(sortedRDDSMA.count()+"\n"); close }

	val addMeanRDD = filterRDD.join(slidingWindow)
	val newMapRDD = addMeanRDD.map(t1=>(t1._1, (t1._2._1._1,(pow(((t1._2._1._2/t1._2._1._3.toFloat)-t1._2._2._2),2)),t1._2._2._3)))
	val newReduceRDD = newMapRDD.reduceByKey((t1, t2)=>(maxString(t1._1, t2._1), t1._2+t2._2, t1._3+t2._3))
	new FileWriter("BB.scala.output.txt",true) { write(newReduceRDD.count()+"\n"); close }
}}
