//@begin_imports
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
//@end_imports

//@begin_methods

object MAXDAY {

  //@end_methods

  //@begin_setup
  def main(args: Array[String]) {

    //@end_setup
    //@begin_spark_prog

    val conf = new SparkConf().setAppName("MAXDAY")
    val sc = new SparkContext(conf)

    val lines = sc.textFile(args(0))

    val dateValueMap = lines.map(line=>(line.split(",")(1), line.split(",")(6).toInt))
    val mapOnes = dateValueMap.map(tuple => (tuple._1, (tuple._2, 1)))
    val reduceRDD = mapOnes.reduceByKey((t1,t2) => (t1._1+t2._1, t2._2+t1._2))
    val maxResult = reduceRDD.max()(new Ordering[Tuple2[String, (Int, Int)]]() {override def compare(x: (String, (Int, Int)), y: (String, (Int, Int))): Int =Ordering[Int].compare(x._2._1, y._2._1)})


    //@end_spark_prog

    println(maxResult)

  }
}