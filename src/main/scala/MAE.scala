//@begin_imports
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
//@end_imports

//@begin_methods

object MAE {

  def generateWindow(tuple:((String, String, Double), Long), windowSize: Int): TraversableOnce[((Int, String),(String, Double, Int))] ={
    val name = tuple._1._1
    val date = tuple._1._2
    val value = tuple._1._3
    val index = tuple._2.toInt
    return for(offset  <- 0 until windowSize) yield
      ((index-offset, name), (date, value,1))
  }

  def maxString(one: String, two: String): String ={
    if(one.compareTo(two)>0)
      return one
    return two
  }

  //@end_methods

  //@begin_setup
  def main(args: Array[String]) {
    val WINDOW_SIZE = 20

    //@end_setup
    //@begin_spark_prog

    val conf = new SparkConf().setAppName("ScalaMAE")
    val sc = new SparkContext(conf)

    val lines = sc.textFile(args(0))
    val dateValueMap = lines.map(line=>(line.split(",")(0), line.split(",")(1), line.split(",")(5).toDouble))
    val putIndex = dateValueMap.zipWithIndex()
    val slidingWindow = putIndex.flatMap(tuple => generateWindow(tuple, WINDOW_SIZE))
    val reduceRDD = slidingWindow.reduceByKey((t1,t2)=>(maxString(t1._1, t2._1),t1._2+t2._2,t1._3+t2._3))
    val filterRDD = reduceRDD.filter(t1=> (t1._2._3 == WINDOW_SIZE))
    val mapRDDMAE = filterRDD.map(t1=>((t1._1._2, t1._2._1), ((t1._2._2/t1._2._3.toDouble)-(t1._2._2/t1._2._3.toDouble)*0.05, (t1._2._2/t1._2._3.toDouble)+(t1._2._2/t1._2._3.toDouble)*0.025)))
    val sortedRDDMAE = mapRDDMAE.sortByKey()

    //@end_spark_prog

  //  for (x <- sortedRDDMAE.collect())
  //    println(x)

    println(sortedRDDMAE.count())

  }
}